---
title: "Home"
site: workflowr::wflow_site
output:
  workflowr::wflow_html:
    toc: false
editor_options:
  chunk_output_type: console
---

Welcome to the research website of the SolFami project.

This is the result for LSU. See the [other site](https://adrientaudiere.gitlab.io/solfami_bioinfo_ssu/) for the SSU result.

To rerun the analysis on linux you need to install [vsearch](https://github.com/torognes/vsearch) as well as the package **default-jdk** (`ŝudo apt-get install -y default-jdk r-base-dev`).
